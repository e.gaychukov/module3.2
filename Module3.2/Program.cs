﻿using System;
using System.Collections.Generic;


namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Task5 task5 = new Task5();
            Console.WriteLine(task5.ReverseNumber(-654321));


            Task8 task8 = new Task8();
            int[,] spiral = task8.FillArrayInSpiral(7);
            for (int i = 0; i < spiral.GetLength(0); i++)
            {
                for (int j = 0; j < spiral.GetLength(1); j++)
                {
                    Console.Write(spiral[i, j] + "\t");
                }
                Console.WriteLine();
            }

        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result) && result >= 0)
            {
                return int.TryParse(input, out result);
            }
            int.TryParse(input, out result);
            return false;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] sequence = new int[Math.Abs(n)];
            int firstNum = 0, secondNum = 1;
            for(int i = 1; i < Math.Abs(n); i++)
            {
                sequence[i] = firstNum + secondNum;
                secondNum = firstNum;
                firstNum = sequence[i]; 
            }
            
            return sequence;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            char[] chars = sourceNumber.ToString().ToCharArray();
            Array.Reverse(chars);
            if (sourceNumber < 0)
            {
                Array.Resize(ref chars, chars.Length - 1);
            }
            return Math.Sign(sourceNumber) * int.Parse(chars);
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] array = new int[Math.Abs(size)];
            Random rand = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(1, 10);
            }
            return array;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            return Array.ConvertAll(source, el => -el);
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] array = new int[Math.Abs(size)];
            Random rand = new Random();
            for (int i = 0; i < array.Length; i++)
                array[i] = rand.Next(1, 10);
            return array;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> list = new List<int>();
            for(int i = 1; i < source.Length; i++)
            {
                if(source[i] > source[i - 1])
                {
                    list.Add(source[i]);
                }
            }
            return list;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            size = Math.Abs(size);
            int h = size;
            int[,] arr = new int[size, size];
            int startValue = 1;
            for (int g = 0; g < (int)Math.Ceiling(size / 2.0); g++, --h)
            {
                Fill(arr, size - h + 1, startValue);
                startValue += 4 * (size - 1 - 2 * (size - h));
            }
            return arr;
        }
        private void Fill(int[,] arr, int num, int startValue)
        {
            num--;
            startValue--;
            int i = num;
            int j = num;
            int n = arr.GetLength(0) - 2 * num;
            for (int k = 1; k <= n * n; k++)
            {
                if (n == 1)
                {
                    arr[num, num] = k + startValue;
                }
                if (k < n)
                {
                    arr[i, j++] = k + startValue;
                    continue;
                }
                if (k < 2 * n - 1)
                {
                    arr[i++, j] = k + startValue;
                    continue;
                }
                if (k < 3 * (n - 1) + 1)
                {
                    arr[i, j--] = k + startValue;
                    continue;
                }
                if (k <= 4 * (n - 1))
                {
                    arr[i--, j] = k + startValue;
                }
            }
        }
    }
}
